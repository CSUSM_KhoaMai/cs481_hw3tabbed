﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//This will generate random quotes for each topic


namespace CS481_HW3Tabbed
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeftPage : ContentPage
    {
        public LeftPage()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MidPage)}:  ctor");
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
            base.OnAppearing();
            Random rnd = new Random();
            int rInt = rnd.Next(0, 3);

            List<string> dogQuotes = new List<string>(new string[] { "A dog is the only thing on earth that loves you more than you love yourself",
                "When the dog looks at you, the dog is not thinking what kind of a person you are.",
                "No matter how you're feeling, a little dog gonna love you." });
            RandDogText.Text = dogQuotes[rInt];
        }
        //update the background for cosmetic
        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
            base.OnDisappearing();
            ((NaviTab)Application.Current.MainPage).BarBackgroundColor = Color.DarkOrchid;

        }
    }
}