﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Tabbed
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NaviTab();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
