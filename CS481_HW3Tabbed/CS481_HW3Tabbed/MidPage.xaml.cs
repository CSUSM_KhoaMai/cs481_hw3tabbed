﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//This will generate random quotes for each topic


namespace CS481_HW3Tabbed
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MidPage : ContentPage
    {
        public MidPage()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MidPage)}:  ctor");
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
            base.OnAppearing();
            Random rnd = new Random();
            int rInt = rnd.Next(0, 3);

            List<string> catQuotes = new List<string>(new string[] { "Time spent with cats is never wasted.",
                "Cats choose us, we don't own them.",
                "It is impossible to keep a straight face in the presence of one or more kittens." });
            RandCatText.Text = catQuotes[rInt];
        }
        //update the background for cosmetic
        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
            base.OnDisappearing();
            ((NaviTab)Application.Current.MainPage).BarBackgroundColor = Color.DarkOrange;
        }
    }
}