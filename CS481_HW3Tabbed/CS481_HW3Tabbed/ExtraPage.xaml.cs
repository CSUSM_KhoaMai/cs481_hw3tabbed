﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//This will generate random quotes for each topic

namespace CS481_HW3Tabbed
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExtraPage : ContentPage
    {
        public ExtraPage()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MidPage)}:  ctor");
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
            base.OnAppearing();
            Random rnd = new Random();
            int rInt = rnd.Next(0, 3);

            List<string> dogQuotes = new List<string>(new string[] { "Give me a fish and I eat for a day. Teach me to fish and I eat for a lifetime.",
            "The biggest fish he ever caught were those that got away.",
                "Net the large fish and you are sure to have the small fry." });
            RandFishText.Text = dogQuotes[rInt];
        }

        //update the background for cosmetic
        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
            base.OnDisappearing();
            ((NaviTab)Application.Current.MainPage).BarBackgroundColor = Color.DarkSalmon;

        }
    }
}