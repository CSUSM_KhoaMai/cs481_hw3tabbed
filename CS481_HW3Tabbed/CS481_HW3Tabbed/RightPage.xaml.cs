﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//This will generate random quotes for each topic


namespace CS481_HW3Tabbed
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RightPage : ContentPage
    {
        public RightPage()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MidPage)}:  ctor");
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
            base.OnAppearing();
            Random rnd = new Random();
            int rInt = rnd.Next(0, 3);

            List<string> birdQuotes = new List<string>(new string[] { "The reason birds can fly and we can't is simply because they have perfect faith, for to have faith is to have wings.",
                "No bird soars too high if he soars with his own wings.",
                "A bird cannot fly with one wing only. Human space flight cannot develop any further without the active participation of women." });
            RandBirdText.Text = birdQuotes[rInt];
        }
        //update the background for cosmetic
        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
            base.OnDisappearing();
            ((NaviTab)Application.Current.MainPage).BarBackgroundColor = Color.DarkOliveGreen;
        }
    }
}